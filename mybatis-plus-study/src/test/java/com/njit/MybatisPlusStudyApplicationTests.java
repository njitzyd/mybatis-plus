package com.njit;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.njit.mapper.UserMapper;
import com.njit.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
class MybatisPlusStudyApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Test
    void contextLoads() {
        // 参数是一个 Wrapper ，条件构造器，这里我们先不用 null
        // 查询全部用户
        List<User> users = userMapper.selectList(null);
        // Lambda表达式
        users.forEach(System.out::println);
    }

    // 测试插入
    @Test
    public void testInsert() {
        User user = new User();
        user.setId(7L);
        user.setName("njitzyd");
        user.setAge(18);
        user.setEmail("njitzyd@qq.com");
        int insert = userMapper.insert(user);
        System.out.println(insert);
        System.out.println(user);
    }

    // 测试更新
    @Test
    public void testUpdate() {
        User user = new User();
        user.setId(1260056765047816197L);
        user.setName("测试自动填充");
        user.setAge(20);
        int i = userMapper.updateById(user);

    }

    //测试乐观锁成功！
    @Test
    public void testOptimisticLocker() {
// 1、查询用户信息
        User user = userMapper.selectById(1L);
// 2、修改用户信息
        user.setName("JPQ");
        user.setEmail("123456789@qq.com");
// 3、执行更新操作
        userMapper.updateById(user);
    }


    // 测试乐观锁失败！多线程下
    @Test
    public void testOptimisticLocker2() {
        // 线程 1
        User user = userMapper.selectById(1L);
        user.setName("JPQKA");
        user.setEmail("qqaz@qq.com");
        // 模拟另外一个线程执行了插队操作
        User user2 = userMapper.selectById(1L);
        user2.setName("123456789");
        user2.setEmail("azqq@qq.com");
        userMapper.updateById(user2);
        userMapper.updateById(user); // 如果没有乐观锁就会覆盖插队线程的值！
    }

    // 测试查询
    @Test
    public void testSelectById() {
        User user = userMapper.selectById(1260056765047816194L);
        System.out.println(user);
    }

    // 测试批量查询！
    @Test
    public void testSelectByBatchId() {
        List<User> users = userMapper.selectBatchIds(Arrays.asList(1, 2, 3));
        users.forEach(System.out::println);
    }

    // 按条件查询之一使用map操作
    @Test
    public void testSelectByBatchIds() {
        HashMap<String, Object> map = new HashMap<>();

        // 自定义要查询
        map.put("name", "njitzyd");
        map.put("age", 18);
        List<User> users = userMapper.selectByMap(map);
        users.forEach(System.out::println);
    }

    // 测试分页查询
    @Test
    public void testPage() {
        // 参数一：当前页
        // 参数二：页面大小
        // 使用了分页插件之后，所有的分页操作也变得简单的！
        Page<User> page = new Page<>(2, 5);
        userMapper.selectPage(page, null);
        page.getRecords().forEach(System.out::println);
//        System.out.println(page.getTotal());
    }

    // 测试删除
    @Test
    public void testDeleteById() {
        userMapper.deleteById(1240620674645544965L);
    }

    // 通过id批量删除
    @Test
    public void testDeleteBatchId() {
        userMapper.deleteBatchIds(Arrays.asList(1240620674645544961L, 1240620674645544962L));
    }

    // 通过map删除
    @Test
    public void testDeleteMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", "Tom");
        userMapper.deleteByMap(map);
    }


    // 测试逻辑删除
    @Test
    public void testLogicDeleteById() {
        userMapper.deleteById(1260056765047816194L);
    }



}
