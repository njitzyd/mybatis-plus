package com.njit.service;

import com.njit.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author njitzyd
 * @since 2020-05-26
 */
public interface UserService extends IService<User> {

}
