package com.njit;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@MapperScan("com.njit.mapper")
@SpringBootApplication
@EnableTransactionManagement
public class MybatisPlusAutogeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisPlusAutogeneratorApplication.class, args);
    }

}
