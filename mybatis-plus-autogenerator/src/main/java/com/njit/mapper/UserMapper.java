package com.njit.mapper;

import com.njit.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author njitzyd
 * @since 2020-05-26
 */
public interface UserMapper extends BaseMapper<User> {

}
