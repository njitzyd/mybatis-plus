package com.njit.web;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author njitzyd
 * @since 2020-05-26
 */
@RestController
@RequestMapping("//user")
public class UserController {

}
